Changelog
=========

Version 0.0.1a2
---------------

*2023.08.06*

- Fix packaging

Version 0.0.1a1
---------------

*2023.07.21*

- Initial version