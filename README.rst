=======
subproj
=======

* PyPI: https://pypi.org/project/subproj/
* Downloads: https://gitlab.com/fholmer/subproj/-/packages
* Documentation: https://fholmer.gitlab.io/subproj
* Source Code: https://gitlab.com/fholmer/subproj
* License: BSD License

Summary
=======

Handle subprojects on-prem.

Warning
=======

* This is a beta version. Not ready for production.


Installation
============

Open command line and and install using pip:

.. code-block:: console

    $ pip install subproj


Usage
=====

subproj is available as console script and library module

.. code-block:: console

    $ subproj -h
    $ python -m subproj -h

Update a project

.. code-block:: console

  $ cd proj
  $ subproj update .
