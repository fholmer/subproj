subproj.setup package
=====================

Submodules
----------

subproj.setup.update module
---------------------------

.. automodule:: subproj.setup.update
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: subproj.setup
   :members:
   :undoc-members:
   :show-inheritance:
