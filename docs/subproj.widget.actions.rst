subproj.widget.actions package
==============================

Submodules
----------

subproj.widget.actions.package\_install module
----------------------------------------------

.. automodule:: subproj.widget.actions.package_install
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: subproj.widget.actions
   :members:
   :undoc-members:
   :show-inheritance:
