.. subproj documentation master file, created by
   sphinx-quickstart on Sun Aug  6 08:58:20 2023.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to subproj's documentation!
===================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   readme
   api_ref
   changelog


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
