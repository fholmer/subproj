subproj.widget.startup package
==============================

Submodules
----------

subproj.widget.startup.app\_checks module
-----------------------------------------

.. automodule:: subproj.widget.startup.app_checks
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: subproj.widget.startup
   :members:
   :undoc-members:
   :show-inheritance:
