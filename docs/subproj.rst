subproj package
===============

Subpackages
-----------

.. toctree::
   :maxdepth: 4

   subproj.setup
   subproj.widget

Submodules
----------

subproj.logging module
----------------------

.. automodule:: subproj.logging
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: subproj
   :members:
   :undoc-members:
   :show-inheritance:
