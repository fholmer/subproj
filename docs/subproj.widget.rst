subproj.widget package
======================

Subpackages
-----------

.. toctree::
   :maxdepth: 4

   subproj.widget.actions
   subproj.widget.startup
   subproj.widget.widgets

Submodules
----------

subproj.widget.tk\_base module
------------------------------

.. automodule:: subproj.widget.tk_base
   :members:
   :undoc-members:
   :show-inheritance:

subproj.widget.tk\_commit module
--------------------------------

.. automodule:: subproj.widget.tk_commit
   :members:
   :undoc-members:
   :show-inheritance:

subproj.widget.tk\_subproj module
---------------------------------

.. automodule:: subproj.widget.tk_subproj
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: subproj.widget
   :members:
   :undoc-members:
   :show-inheritance:
